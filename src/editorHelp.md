#Editor Documentation

Ctrl/Cmd & O: *open file*
Ctrl/Cmd & N: *new file*
Ctrl/Cmd & S: *save file*
F3: *test map (map must have been saved once, autosaves on test thereafter)*

WASD: *scroll map*
Ctrl/Cmd & LMB: *paint currently selected tile*
Ctrl/Cmd & RMB: *sample tile to selection*

Tab (hold): *display quick menu*

Escape: *quit*

The editor will store a single backup file (RESTORE.map) when performing the following operations: quitting or creating a new file.
