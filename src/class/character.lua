--[[
	Character
	Entities capable of voluntary movement.
]]

require "class.entity"

class "Character" (Entity) {
	__init__ = function(self, spriteset, pos, name)
		Entity.__init__(self, spriteset, pos, name)

		self.jump = -290 --jump velocity
		self.jumpCount = 1
		self.maxJumps = 1
		self.air = true
		self.jumpDisble = true

		--input
		self.control = {}
	end,

	step = function(self, dt, map)
		local input = vector()
		--local env = map:getEnv(self.pos)!!

		--allow character to fall a tiny bit before disallowing jumps
		if self.jumpCountTime then
			self.jumpCountTime = self.jumpCountTime > 0 and self.jumpCountTime - dt
			self.jumpCount = self.jumpCountTime and self.jumpCount or self.jumpCount + 1
		end


		--voluntary movement
		if not self.controlLock then
			--make sure there is room to move upwards
			local tileSize = map:getTileSize()
			local vec = vector(self.box.w /2, -1)
			local vec2 = vector(4, -1)
			local tile1, tile2 = map:getTile(self.pos + vec), map:getTile(self.pos + vec2)

			--check for directional input
			input.y = self.control.up and input.y - 1 or input.y
			input.y = self.control.down and input.y + 1 or input.y
			input.x = self.control.left and input.x - 1 or input.x
			input.x = self.control.right and input.x + 1 or input.x

			input.y = (tile1.prop.pass or tile2.prop.pass) and 0 or input.y

			self.facing = input.x ~= 0 and input.x or self.facing

			input:normalize_inplace()
			self.vel = self.vel + input * self.accel * dt

			--stay within max values
			self.vel.x = util.clamp(-self.maxVel, self.vel.x, self.maxVel)
			self.vel.y = util.clamp(-self.maxVelY, self.vel.y, self.maxVelY)

			if input.x == 0 then
				self:applyFriction(dt)
			end

			--check for jumping
			if self.control.jump and self:canJump() then
				if not tile1.prop.pass then
					self.vel.y = self.jumpDisable and self.vel.y or self.jump
					self.jumpDisable = true
					self.air = true
					self.jumpCount = self.jumpCount + 1
					self.control.jump = false
					self.sprite:setAnim("jump", true)
				end
			end

			if self.control.jumpRelease then
				self.vel.y = math.max(-100, self.vel.y)
				self.jumpDisable = false
				self.control.jump = false
				self.control.jumpRelease = false
			end
		else
			self.controlLock = self.controlLock > 0 and self.controlLock - dt
			if self.controlLock then
				self.sprite:setColor(255, 100, 100)
			else
				self.sprite:setColor()
			end

			self:applyFriction(dt)
		end

			self:applyGravity(dt)

			--update position
			self.pos = self.pos + self.vel * dt
	end,

	canJump = function(self)
		return not self.air or self.jumpCount < self.maxJumps
	end,

	hasLanded = function(self)
		self.jumpCount = 0
		Entity.hasLanded(self)
	end,

	hasFallen = function(self)
		self.jumpCountTime = 0.1
		Entity.hasFallen(self)
	end,

	--damage handling
	takeDmg = function(self, amt, dir)
		self.hp = self.hp - amt
	end,

	takeDmgTime = function(self, amt, vec, time)
		amt = amt or 1
		time = time or 0.5

		if self.dmgTimer <= 0 then
			self.dmgTimer = time
			self.controlLock = 0.1
			self.hp = self.hp - amt
			--self.vel = vec * 10
			print "hit!"
		end
	end,

	collideWithTile = function(self, tile, tileSize)
		--[[
			spawn particles and play sounds based on tile material
			take damage, adjust temperature, etc
		]]
		--find distance vector
		local half = vector(tileSize / 2, tileSize / 2)
		local vec = self.pos + vector(self.box.w / 2, self.box.h / 2) - (tile.pos + half)
		-- local vec = self.pos - tile.pos

		if tile.prop.dmg then
			self:takeDmgTime(tile.prop.dmg, vec)
		end
	end


}