--[[
	Map
	Contains level data.
]]

class "Map" {
	__init__ = function(self, file)
		util.assertArg("Map", 1, "string", file)
		file = love.filesystem.load(file)()

		--basic data
		self.tileset = cache.tileset(file.tileset)
		self.layout = file.layout --table of tiles
		self.walk = file.walk --layer that collides with player
		self.width = file.width
		self.height = file.height
		self.parallax = {} --todo
		self.batch = {}

		self:renderBatches()

		--dynamic elements
		self.mapEnt = {} --todo

		--drawing properties
		self.pos = vector()
		self.offset = vector()
		self.color = {255, 255, 255, 255}
	end,

	update = function(self, dt)
		for _, ent in ipairs(self.mapEnt) do
			ent:update(dt)
		end
	end,

	draw = function(self, allLayers)
		allLayers = allLayers and #self.batch or self.walk
		for layer = 1, allLayers do
			love.graphics.setColor(self.color)
			love.graphics.draw(self.batch[layer], self.pos.x, self.pos.y, 0, 1, 1, self.offset.x, self.offset.y)
		end
	end,

	drawForeground = function(self)
		for layer = self.walk + 1, #self.batch do
			love.graphics.setColor(self.color)
			love.graphics.draw(self.batch[layer], self.pos.x, self.pos.y, 0, 1, 1, self.offset.x, self.offset.y)
		end
	end,

	renderBatches = function(self)
	--render map layers
		local bsize = self.width * self.height
		local tileSize = self.tileset.tileSize

		for z = 1, #self.layout do
			--tile layers
			self.batch[z] = love.graphics.newSpriteBatch(self.tileset.img, bsize)
			local layer = self.batch[z]
			layer:bind()
			for y = 0, self.height - 1 do
				for x = 0, self.width - 1 do
					local t = self.layout[z][y + 1][x + 1]
					local q = self.tileset:getQuad(t)
					layer:add(q, x * tileSize, y * tileSize)
				end
			end
			layer:unbind()
		end
	end,

	getTileSize = function(self)
		return self.tileset.tileSize
	end,

	--retrieve tile properties and append world coords, takes world coords
	getTile = function(self, t, z)
		z = z or self.walk
		local tileSize = self.tileset.tileSize
		--get tile coords
		local x, y = t:unpack()
		x, y = math.floor(x / tileSize), math.floor(y / tileSize)
		x, y = util.clamp(0, x, self.width - 1), util.clamp(0, y, self.height + 1)

		--get tile properties from tileset
		local id = 1
		local mapx, mapy = x + 1, y + 1 
		if self.layout[z] and self.layout[z][mapy] and self.layout[z][mapy][mapx] then
			id = self.layout[z][mapy][mapx]
		end

		local tile = self.tileset:getTile(id)
		tile.pos = vector(x * tileSize, y * tileSize) --add the coords of this particular tile

		return tile
	end,

	--retrieve table of tiles
	getTiles = function(self, t, z)
		local result = {}
		for _, v in ipairs(t) do
			table.insert(result, self:getTile(v, z))
		end
		return result
	end

}