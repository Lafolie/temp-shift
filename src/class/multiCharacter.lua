require "class.entity"
require "class.character"

class "multiCharacter" {
	__init__ = function(self, defaultChar, ...)
		rawset(self, "charList", {...})
		rawset(self, "currentChar", self.charList[defaultChar])
	end,

	__setattr__ = function(self, key, value)
		self.currentChar[key] = value
	end,

	__getattr__ = function(self, key)
		return self.currentChar[key]
	end,

	switchChar = function(self, char)
		local newChar = self.charList[char]
		newChar.pos = self.currentChar.pos:clone()
		newChar.vel = self.currentChar.vel:clone()
		rawset(self, "currentChar", self.charList[char])
	end
}