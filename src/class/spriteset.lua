--[[
	Spriteset
	Metadata for sprites, handles quad, collision box and animation data for Sprites.
	Animations are specified as as a table containing string indexed animation tables.
	Each individual animation contains a table contain a quad ref and a timer.
	The timer value can also be a string to specify an animation to switch to, or "stop" to halt animation.
]]

local idleAnim ={
	idle = {
		{quad = 1, time = "stop"}
	}
}

class "Spriteset" {
	__init__ = function(self, file)
		util.assertArg("Spriteset", 1, "string", file)
		file = love.filesystem.load(file)()

		self.img = cache.image(file.img)
		self.imgWidth, self.imgHeight = self.img:getWidth(), self.img:getHeight()
		self.width, self.height = file.width, file.height
		self.anim = file.anim or idleAnim
		self.box = file.box or {x = 0, y = 0, w = self.width, h = self.height}
		self.pad = file.pad

		--generate cel quads
		self.quad = {}
		local imgW, imgH = self.imgWidth, self.imgHeight
		local w, h = self.width, self.height
		local pad = self.pad
		local pad2 = math.ceil(pad / 2)
		local gridW, gridH = w + pad, h + pad

		for y = 0, (imgH / gridH) - 1 do
			for x = 0, (imgW / gridW) - 1 do
				local q = love.graphics.newQuad(x * gridW + pad2, y * gridH + pad2, w, h, imgW, imgH)
				table.insert(self.quad, q)
			end
		end
	end,

	getQuad = function(self, id)
		return self.quad[id]
	end
}