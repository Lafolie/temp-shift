--[[
	Sprite
	Visual component for Ents. Capable of animation.
	Frame = animation index
	Cel = animation cel == anim[frame]
]]

class "Sprite" {
	__init__ = function(self, file, pos)
		util.assertArg("Sprite", 1, "string", file)
		self.spriteset = cache.spriteset(file)

		--position
		self.pos = pos or vector()
		self.offset = vector()
		self.rotation = 0
		self.scale = vector(1, 1)
		self.shear = vector()

		--animation
		self.animSpeed = 1
		self:setAnim("idle")

		--draw misc
		self.img = self.spriteset.img
		self.blend = "alpha" --unused
		self.color = {255, 255, 255, 255}

	end,

	update = function(self, dt)
		--update animation
		if type(self.timer) == "number" then
			self.timer = self.timer - self.animSpeed * dt
			if self.timer <= 0 then
				self:nextFrame()
			end
		elseif self.timer ~= "stop" then
			self:setAnim(self.timer) --switch to new anim
		end
	end,

	draw = function(self)
		love.graphics.setColor(self.color)
		love.graphics.draw(self.img, self.quad, self.pos.x, self.pos.y, self.rotation, self.scale.x, self.scale.y, self.offset.x, self.offset.y, self.shear.x, self.shear.y)
	end,

	setAnim = function(self, newAnim, reset)
		newAnim = self.spriteset.anim[newAnim] or self.spriteset.anim.idle
		--assert(newAnim, "Specified animation does not exist.")
		if reset or newAnim ~= self.anim then
			self.anim = newAnim
			self.frame = 0
			self:nextFrame()
		end
	end,

	nextFrame = function(self)
		self.frame = self.frame < #self.anim and self.frame + 1 or 1
		self.cel = self.anim[self.frame]
		self.quad = self.spriteset:getQuad(self.cel.quad)
		self.timer = self.cel.time
	end,

	getWidth = function(self)
		return self.spriteset.width
	end,

	getHeight = function(self)
		return self.spriteset.height
	end,

	setColor = function(self, r, g, b, a)
		r, g, b = r or 255, g or 255, b or 255
		a = a or self.color[4]
		self.color = {r, g, b, a}
	end,

	setAlpha = function(self, a)
		a = a or 255
		self.color[4] = a
	end,

	flipX = function(self, dir)
		self.scale.x = dir
		self.offset.x = dir == -1 and self.spriteset.width or 1
	end,

	flipY = function(self, dir)
		self.scale.y = dir
		self.offset.y = dir == -1 and self.spriteset.height or 1
	end,

	getWidth = function(self)
		return self.spriteset.width
	end,

	getHeight = function(self)
		return self.spriteset.height
	end,

	getCollisionOffset = function(self)
		return vector(self.spriteset.box.x, self.spriteset.box.y)
	end
}