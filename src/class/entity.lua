--[[
	Entity
	Base class for things that exist in the game world.
	Sprite collision boxes must be multiples of the tile size!
]]

class "Entity" {
	__init__ = function(self, spriteset, pos, name)
		util.assertArg("Entity", 1, "string", spriteset)
		util.assertArg("Entity", 2, "table", pos)
		self.sprite = Sprite(spriteset, pos)

		--statistics
		self.name = name or "New Entity"
		self.hp = 3
		self.dmgTimer = 0 --used to regulate damage

		--pseudo physics
		self.pos = pos
		self.vel = vector()
		self.maxVel = 125
		self.maxVelY = 300
		self.accel = 1000
		self.decel = 1000
		self.elastic = 0.10
		
		self.facing = 1
		self.box = self:getCollisionBox()

		self.sprite:setAnim("run")

		--responsibilities
		self.particle = {}

		--debug
		self.dbug = {showCollisionBox = true}
	end,

	update = function(self, dt, map)
		--returns
		self.kill = false --remove from ent stack
		self.newEnts = {} --insert into ent stack

		--status
		self.dmgTimer = self.dmgTimer - dt

		--pseudo physics simulation
		self:step(dt, map)
		self:collide(dt, map)

		--update graphical components
		self:updateSprite(dt)
		self:updateParticles(dt)

		--finish up
		self:isAlive()
		return self.kill, self.newEnts
	end,

	draw = function(self)
		self.sprite:draw()
		
	-- 	love.graphics.print(self.jumpCount, 150, 1)
		-- love.graphics.print("X: " .. self.vel.x .. "\nY: " .. self.vel.y, self.pos.x, self.pos.y - 50)
		-- if self.dbug.tiles then
		-- 	love.graphics.setColor(50, 200, 50, 75)
		-- 	for _, t in ipairs(self.dbug.tiles) do
		-- 		love.graphics.rectangle("fill", t.pos.x, t.pos.y, 16, 16)
		-- 	end
		-- end

		-- if self.dbug.showCollisionBox then
		-- 	local pos = self.pos
		-- 	love.graphics.setColor(200, 50, 50, 75)
		-- 	love.graphics.rectangle("fill", pos.x, pos.y, self.box.w, self.box.h)
		-- 	love.graphics.rectangle("line", self.sprite.pos.x, self.sprite.pos.y, self.sprite:getWidth(), self.sprite:getHeight())
		-- end
		
	-- 	if lineCheck then
	-- 		for k, v in ipairs(lineCheck) do
	-- 			love.graphics.line(v, 0, v, 600)
	-- 		end
	-- 	end
	-- 	if line then
	-- 		love.graphics.line(0, line, 800, line)
	-- 	end
	end,

	toggleControl = function(self)
		self.controlLock = not self.controlLock
	end,

	--physics methods
	step = function(self, dt, map)
		--local env = map:getEnv(self.pos)!!

		--stay within max values
		self.vel.x = util.clamp(-self.maxVel, self.vel.x, self.maxVel)
		self.vel.y = util.clamp(-self.maxVelY, self.vel.y, self.maxVelY)

		self:applyFriction(dt)
		self:applyGravity(dt)

		--update position
		self.pos = self.pos + self.vel * dt

	end,

	applyFriction = function(self, dt)
		local friction = self.decel --temporary!!
		if self.vel.x > 0 then
			self.vel.x = self.vel.x > friction * dt and self.vel.x - friction * dt or 0
		elseif self.vel.x < 0 then
			self.vel.x = self.vel.x < -friction * dt and self.vel.x + friction * dt or 0
		end
	end,

	applyGravity = function(self, dt)
		--gravitah
		if self.air then
			local gravity = 800
			self.vel.y = self.vel.y + gravity * dt
		end
	end,

	bounceX = function(self)
			self.vel.x = -self.vel.x * self.elastic
			self.vel.x = math.abs(self.vel.x) < 30 and 0 or self.vel.x
	end,

	bounceY = function(self)
		self.vel.y = -self.vel.y * self.elastic
		self.vel.y = math.abs(self.vel.y) < 70 and 0 or self.vel.y
	end,

	hasLanded = function(self)
		self.air = false
		self:bounceY()
		-- self.vel.y = 0
	end,

	hasFallen = function(self)
		self.air = true
	end,

	--coolision methods
	collide = function(self, dt, map)
		--[[
			check whether y or x velocty is higher
			check which direction moving in
			get list of appropriate tiles
			check collision with each one, fire events as necessary
			adjust accordingly
		]]
		self.dbug.tiles = {}
		--determine which axis to handle first
		if self.vel.x > self.vel.y then
			self:collideMapX(map)
			self:collideMapY(map)
		else
			self:collideMapY(map)
			self:collideMapX(map)
		end
	end,

	collideMapX = function(self, map)
		--determine amount of tiles that need to be checked
		local tileSize = map:getTileSize()
		local tileSizeHalf = tileSize * 0.5
		local air = self.vel.y < 0 and 1 or 0 --check an extra tile when jumping
		local num = math.floor(self.box.h / tileSize) + air --one day, refactor codebase to make .spr width/heights unit in tiles
		--offsets
		
		local left = vector(0, 1)
		local right = vector(self.box.w, 1)

		--generate list of tiles to check
		local check = {}
		for i = 0, num do
			left.y = i * tileSize
			right.y = i * tileSize

			table.insert(check, map:getTile(self.pos + left))
			table.insert(check, map:getTile(self.pos + right))
		end

		--check an additional tile (on both sides) if airborne 

		local nextTile
		local hit = 0
		left.x, left.y = tileSize, 0 --may as well reuse this
		right.x, right.y = -tileSize, 0
		for _, tile in ipairs(check) do
			--local i = self.vel.y > 0 and #check + 1 - i or i
			
			table.insert(self.dbug.tiles, tile) --debug
			--check collision properties and adjust accordingly
			if tile.prop.pass then
				if (tile.pos.y < self.pos.y + self.box.w or (tile.pos.y > self.pos.y and self.pos.y + self.box.h > tile.pos.y + 4))
				and tile.pos.y > self.pos.y - 2 then
					if tile.pos.x > self.pos.x and self.vel.x > 0 then
						--right wall collision!
						nextTile = map:getTile(tile.pos + right)
						if not nextTile.prop.pass then
							self.pos.x = tile.pos.x - self.box.w
							self:collideWithTile(tile, tileSize)
						end
						hit = hit + 1
					elseif tile.pos.x < self.pos.x and self.vel.x < 0 then
						--left wall collision!
						nextTile = map:getTile(tile.pos + left)
						if not nextTile.prop.pass then
							self.pos.x = tile.pos.x + tileSize
							self:collideWithTile(tile, tileSize)
						end
						hit = hit + 1
					end
				end
			end
		end
		if hit > 0 then self:bounceX() end
	end,

	--needs refactoring as floor collision is now handled differently than originally intended!!
	collideMapY = function(self, map)
		--determine tiles that need to be checked
		local ground = 0
		local tileSize = map:getTileSize()
		local tileSizeHalf = tileSize * 0.5
		local num = math.ceil(self.box.w / tileSize)
		--offsets
		local ceil = vector(0, -1)
		local floor = vector(0, self.box.h)

		lineCheck = {}
		--generate list of tiles to check
		local check = {}
		for i = 0, num do
			
			local j --make ent thinner for y collisions (to fit through small gaps)
			j = i == 0 and 4 or 0
			j = i == num and -4 or j
			ceil.x = i * tileSize + j
			floor.x = i * tileSize + j

			table.insert(check, map:getTile(self.pos + ceil))
			table.insert(check, map:getTile(self.pos + floor))
			table.insert(lineCheck, self.pos.x + floor.x)
		end

		local nextTile
		ceil.x, ceil.y = 0, tileSize --may as well reuse this
		for _, tile in ipairs(check) do
			table.insert(self.dbug.tiles, tile)
			--check collision properties and adjust accordingly
			if tile.prop.pass then
				if tile.pos.y < self.pos.y then
					--ceiling collision!
					nextTile = map:getTile(tile.pos + ceil)
					--only collide if position exceeds ceiling tile boundary
					if not nextTile.prop.pass and self.vel.y < 0 then --check velocity to prevent ceiling snappage
						self.pos.y = tile.pos.y + tileSize + 1
						self:bounceY()
						self:collideWithTile(tile, tileSize)
					end
				elseif tile.pos.y > self.pos.y then
					--floor collision!
					nextTile = map:getTile(tile.pos - ceil)
					if not nextTile.prop.pass and self.vel.y >= 0 then
						if not line then line = self.pos.y end
						self.pos.y = tile.pos.y - self.box.h
						ground = ground + 1
						self:collideWithTile(tile, tileSize)
					end
				end
			end
		end

		--check to see if the entity has landed or fallen
		if self.air and ground > 0 then
			self:hasLanded()
		elseif ground == 0 and not self.air then
			self:hasFallen()
		end
	end,

	getCollisionBox = function(self)
		return self.sprite.spriteset.box
	end,

	--sprite-handling methods
	detAnim = function(self)
		--determine animation
		if not self.air then
			--on the ground
			local direction = self.vel:normalized().x
			if direction == 0 then
				self.sprite:setAnim("idle")
			else
				self.sprite:setAnim("run")
			end
		else
			self.sprite:setAnim("jump")
		end
	end,

	updateSprite = function(self, dt)
		self:detAnim()
		self.sprite:flipX(self.facing)
		self.sprite.pos = self.pos - self.sprite:getCollisionOffset() --!!
		self.sprite:update(dt)
	end,

	--misc
	isAlive = function(self)
		if self.hp <= 0 then
			self.dead = true
			if #self.particle == 0 then self.kill = true end --upvalue flag for removal
		end
	end,

	--particle stuff
	updateParticles = function(self, dt)
		for i = #self.particle, 1, -1 do
			local p = self.particle[i]
			local done = p:update(dt, self.dead) --send dead flag to halt particle system
			table.remove(self.particle, p)
		end
	end,

	spawnParticle = function(self, ...)
		--stuff
	end,

	--event handling
	collideWithEnt = function(self, ent)
	end,

	collideWithTile = function(self, tile, tileSize)
	end

}