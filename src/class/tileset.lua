--[[
	Tileset
	Tile data for use in maps.
	Tile properties:
		quad - vec2 - tilesheet coord
		pass - true/false/{vec2, vec2} - passability data
		mat - string - material for particles/sfx
		temp - int - temperature modifier
]]

class "Tileset" {
	__init__ = function(self, file)
		util.assertArg("Tileset", 1, "string", file)
		file = love.filesystem.load(file)()
		self.name = file.name
		self.img = cache.image(file.img)
		self.tile = file.tile
		self.tileSize = file.tileSize
		self.pad = file.pad
		
		self.quad = {}
		local pad = self.pad
		local pad2 = math.ceil(pad / 2)
		local size = self.tileSize
		local grid = size + pad
		local imgW, imgH = self.img:getWidth(), self.img:getHeight()

		for y = 0, (imgH / grid) - 1 do
			self.quad[y + 1] = {}
			for x = 0, (imgW / grid) - 1 do
				self.quad[y + 1][x + 1] = love.graphics.newQuad(x * grid + pad2, y * grid + pad2, size, size, imgW, imgH)
			end
		end
	end,

	getQuad = function(self, id)
		local u, v = unpack(self.tile[id].quad)
		return self.quad[v][u]
	end,

	getTile = function(self, id)
		return {prop = self.tile[id]}
	end
}