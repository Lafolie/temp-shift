--[[
	Edit - Map Editor
	Hacky, horrible thing to make map dev easier.
]]

class "Edit" (State) {
	__init__ = function(self)
		--init stuff
		self.log = {}
		self:setTitle()
		self.tilesetName = "platformy.tset"

		--load appropriate map
		if not persist.mapName then 
			self:loadMap("blank.map")
		else
			self:loadMap(persist.mapName)
		end
		

		--tools
		self.paintTile = 2
		self.cursor = vector()
		self.select = vector()
		self.previousSelect = vector(-1, -1) --initial value of -1 enables drawing in top-left corner on startup
		self.selectBox = vector()
		self.cam = vector()
		self.layer = 1

		--menu
		self.menu = {
			display = false,
			pos = vector(),
			w = 200,
			h = 400
		}

		--settings
		self.tileSize = self.map:getTileSize()
		self.modKey = love.system.getOS() == "OS X" and "lgui" or "lctrl" --use cmd key instead of ctrl on OS X
		if not love.filesystem.isDirectory(love.filesystem.getSaveDirectory() .. "/map") then
			love.filesystem.createDirectory("map") --create map dir
		end
		self:changeLayer(self.map.walk)

		--misc
		love.keyboard.setKeyRepeat(true)
		self.text = ""
		love.graphics.setBackgroundColor(0, 0, 0, 255)
		self.blinkTimer = 0


		--input functions
		self.input = {
			w = function(self) self.cam.y = self.cam.y - self.tileSize end,
			a = function(self) self.cam.x = self.cam.x - self.tileSize end,
			s = function(self) self.cam.y = self.cam.y + self.tileSize end,
			d = function(self) self.cam.x = self.cam.x + self.tileSize end,
			f3 = function(self)
				if self.mapName then
					persist.mapName = self.mapName
					love.keyboard.setKeyRepeat(false)
					self:saveMap()
					love.event.push("changeState", "test")
				else
					self:msg("ERROR: Map must be saved before testing.")
				end
			end,
			escape = function(self)
				--self:saveMap()
				love.event.push("quit")
			end,
			tab = function(self)
				self.menu.display = true
			end,
			right = function(self)
				self:increaseSizeX()
				self:changeLayer(self.layer)
			end,
			down = function(self)
				self:increaseSizeY()
				self:changeLayer(self.layer)
			end,
			left = function(self)
				if self.map.width > 1 then
					self:decreaseSizeX()
					self:changeLayer(self.layer)
				else
					self:msg "ERROR: Width must be at least 1!"
				end
			end,
			up = function(self)
				if self.map.height > 1 then
					self:decreaseSizeY()
					self:changeLayer(self.layer)
				else
					self:msg "ERROR: Height must be at least 1!"
				end
			end
		}
	end,

	update = function(self, dt)
		--maoz
		local mx, my = love.mouse.getPosition()
		if not self.menu.display then
			self.cursor.x = math.floor(mx / self.tileSize) * self.tileSize
			self.cursor.y = math.floor(my / self.tileSize) * self.tileSize
			self.select.x = (self.cursor.x - self.cam.x) / self.tileSize
			self.select.y = (self.cursor.y - self.cam.y) / self.tileSize		
			self.menu.pos.x, self.menu.pos.y = mx, my
		end

		--taskings
		if self.task then
			self.menu.display = false
			--save map
			if self.task == "save" then
				if love.keyboard.isDown("lshift") then
					--save as, clear mapname (cause mapName is used in the most spammy way)
					self.mapName = false
				end
				if self.enteredText then
					self.mapName = self.enteredText
					self.enteredText = false
					love.keyboard.setTextInput(false)
					self:saveMap()
					self.task = false
					self.text = ""
				else
					love.keyboard.setTextInput(true)
				end
			elseif self.task == "new" then
				self:saveMap("RESTORE.map")
				self:loadMap("blank.map")
				self:changeLayer(1)
				self.mapName = false
				self.task = false
				self.text = ""
			elseif self.task == "open" then
				if self.enteredText then
					self.mapName = self.enteredText
					self.enteredText = false
					love.keyboard.setTextInput(false)
					self:loadMap()
					self.task = false
					self.text = ""
				else
					love.keyboard.setTextInput(true)
				end
			end
		else

			--no task
			if self.paintMode and
				self.select.x >= 0 and 
				self.select.x < self.map.width and
				self.select.y >= 0 and 
				self.select.y < self.map.height and
				(self.select.x ~= self.previousSelect.x or
				self.select.y ~= self.previousSelect.y)
			then
				--paint a tile to layout
				self.map.layout[self.layer][self.select.y + 1][self.select.x + 1] = self.paintTile
				self.canvas:renderTo(function() self:paintTileToCanvas() end)
				self.previousSelect.x = self.select.x
				self.previousSelect.y = self.select.y
			end
		end

		--text logoggalog
		for y = #self.log, 1, -1 do
			local msg = self.log[y]
			msg[2] = msg[2] - 100 * dt
			if msg[2] < 0 then
				table.remove(self.log, y)
			end
		end

		--misc
		self.blinkTimer = self.blinkTimer < 0 and 1 or self.blinkTimer - dt
		self.caret = self.blinkTimer < 0.5 and "" or "_"
	end,

	draw = function(self)
		love.graphics.push()
		love.graphics.translate(self.cam.x, self.cam.y)
			love.graphics.setColor(50, 150, 200)
			love.graphics.rectangle("fill", 0, 0, self.map.width * self.tileSize, self.map.height * self.tileSize)
			self.map:draw(true)
			love.graphics.draw(self.canvas, 0, 0)
		love.graphics.pop()
		--cursor
		love.graphics.rectangle("line", self.cursor.x, self.cursor.y, self.tileSize, self.tileSize)
		--text entry
		if love.keyboard.hasTextInput() then
			love.graphics.setColor(0, 0, 0, 127)
			love.graphics.rectangle("fill", 0, 0, config.screen.width, 17)
			love.graphics.setColor(0, 0, 0, 255)
			love.graphics.print(self.prompt .. self.text .. self.caret, 1, 1)
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.print(self.prompt .. self.text .. self.caret)
		end	

		--menu
		if self.menu.display then
			love.graphics.setColor(20, 20, 20, 229)
			love.graphics.rectangle("fill", self.menu.pos.x, self.menu.pos.y, self.menu.w, self.menu.h)
		end

		--log
		for y = #self.log, 1, -1 do
			local msg = self.log[#self.log - y + 1]
			love.graphics.setColor(255, 255, 255, msg[2] <= 255 and msg[2] or 255)
			love.graphics.print(msg[1], 1, config.screen.height - y * 15)
		end
		
	end,

	quit = function(self)
		self:saveMap("RESTORE.map")
	end,

	keypressed = function(self, key, unicode)
		if not love.keyboard.hasTextInput() and not self.paintMode then
			--saving, loading
			if love.keyboard.isDown(self.modKey) then
				--modified input
				if key == "s" then
					if not self.mapName then
						self.task = "save"
						self.prompt = "Filename: "
					else
						self:saveMap()
					end
				elseif key == "n" then
					self.task = "new"
				elseif key == "o" then
					self.task = "open"
					self.prompt = "Filename: "
					self.mapName = false
				end
			else
				if self.input[key] then
					local f = self.input[key]
					f(self)
				else
					--change layer
					if tonumber(key) then
						key = key == "0" and 10 or tonumber(key)
						self:changeLayer(key)
						self:msg("Editing layer " .. key)
					end
				end
			end
		elseif not self.paintMode then
			if key == "backspace" then
				self.text = love.keyboard.hasTextInput() and self.text:sub(1, -2) or self.text
			elseif key == "return" then
				self.enteredText = self.text
				self.text = ""
			elseif key == "escape" then
				self.text = ""
				love.keyboard.setTextInput(false)
				self.task = false
			end
		end
	end,

	keyreleased = function(self, key)
		if not love.keyboard.hasTextInput() and not love.keyboard.isDown(self.modKey) then
			if key == "tab" then
				self.menu.display = false
			elseif key == "left" or key == "right" then
				self:msg("Map width is now: " .. self.map.width)
			elseif key == "up" or key == "down" then
				self:msg("Map height is now: " .. self.map.height)
			end
		end
	end,

	mousepressed = function(self, x, y, button)
		if button == "l"  and love.keyboard.isDown(self.modKey) and not self.task then
			self.paintMode = true
		end
	end,

	mousereleased = function(self, x, y, button)
		if button == "l" then
			self.paintMode = false
		end
	end,

	textinput = function(self, text)
		self.text = self.text .. text
	end,

	paintTileToCanvas = function(self)
		local quad = self.map.tileset:getQuad(self.paintTile)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.draw(self.map.tileset.img, quad, self.select.x * self.tileSize, self.select.y * self.tileSize)
	end,

	getTileFromLayout = function()
	end,

	changeLayer = function(self, layer)
		self.layer = layer or self.map.walk
		--self.map:renderBatches()
		--self.canvas = love.graphics.newCanvas(self.map.width * self.tileSize, self.map.height * self.tileSize)
		--generate layer if required
		if not self.map.layout[self.layer] then
			self.map.layout[self.layer] = {}
			for i = 1, self.map.height do
				self.map.layout[self.layer][i] = {} --these need to exist too!
			end
		end
		self.map:renderBatches()
		self:newCanvas()
	end,

	loadMap = function(self, mapName)
		mapName = mapName or self.mapName
		if love.filesystem.exists("map/" .. mapName) then
			self.map = Map("map/" .. mapName)
			self:setTitle()
			self:msg("Loaded file " .. mapName)
			self.tileSize = self.map:getTileSize()
			self:changeLayer()
			self:newCanvas()
			self.mapName = mapName ~= "blank.map" and mapName
		else
			self:msg("ERROR: Could not find " .. mapName)
		end
	end,

	saveMap = function(self, mapName)
		mapName = mapName or self.mapName
		local output = {"local map = {}\n"}
		local ins = function(v)
			table.insert(output, v .. "\n")
		end

		ins(string.format("map.tileset = %q", self.tilesetName))
		ins(string.format("map.walk = %d", self.map.walk))
		ins(string.format("map.width, map.height = %d, %d", self.map.width, self.map.height))

		ins("map.layout = {")
		for _, z in ipairs(self.map.layout) do
			ins("\t{")
			for _, y in ipairs(z) do
				ins("\t\t{" .. table.concat(y, ",") .. "},")
			end
			ins("\t},")
		end
		ins("}\nreturn map")

		local file, err = love.filesystem.newFile("map/" .. mapName, "w")
		if not err then
			for _, line in ipairs(output) do
				file:write(line)
			end
			file:close()
			self:changeLayer(self.layer)
			self:setTitle()
			self:msg("Saved " .. mapName)
			self.map:renderBatches()
		else
			self:msg("ERROR: Could not create file " .. mapName)
		end
	end,

	setTitle = function(self)
		love.window.setTitle("TempShift - Edit - " .. (mapName or "untitled"))
	end,

	msg = function(self, text)
		table.insert(self.log, {text, 500})
	end,

	newCanvas = function(self)
		self.canvas = love.graphics.newCanvas(self.map.width * self.tileSize, self.map.height * self.tileSize)
	end,

	increaseSizeX = function(self)
		for _, z in ipairs(self.map.layout) do
			for _, y in ipairs(z) do
				table.insert(y, 1)
			end
		end
		self.map.width = self.map.width + 1
	end,

	increaseSizeY = function(self)
		for _, z in ipairs(self.map.layout) do
			table.insert(z, self:emptyRow())
		end
		self.map.height = self.map.height + 1
	end,

	emptyRow = function(self)
		local row = {}
		for i = 1, self.map.width do
			table.insert(row, 1)
		end
		return row
	end,

	decreaseSizeX = function(self)
		for _, z in ipairs(self.map.layout) do
			for _, y in ipairs(z) do
				table.remove(y, #y)
			end
		end
		self.map.width = self.map.width - 1
	end,

	decreaseSizeY = function(self)
		for _, z in ipairs(self.map.layout) do
			table.remove(z, #z)
		end
		self.map.height = self.map.height - 1
	end
}

return Edit