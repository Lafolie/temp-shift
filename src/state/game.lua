--[[
	Game state.
	Main gameplay state.
]]

class "Game" (State) {
	__init__ = function(self)
		--world config
		self.tileSet = Tileset("platformy.tset")

		--load world
		self.map = self.loadMap("test2.map")

		local playerStart = self.map:getPlayerStart()
		self.player = Player(playerStart)
		self.entity = self.map:spawnEntities()
		self.entityDraw = {}

		self.cam = vector()
	end,

	update = function(self, dt)
		--update
		self.map:update(dt)
		self:updateEntities(dt, self.map, self.player)
		self.player:update(dt, self.map)

		self:updateCam()
	end,

	draw = function(self)
		love.graphics.pop()
		love.graphics.scale(persist.config.scale)
			--draw previous map if scrolling to new area
			if self.scrollMap then
				self.scrollMap:draw()
			end
			self.map:draw() --draw <= walkable layer
			self:drawEntities()
			self.player:draw()
			self.map:drawForeground()
			--draw hud

		love.graphics.push()

	end,

	--entity funcs
	updateEntities = function(self, dt, map, player)
		self.entityDraw = {}
		local min = self.player.pos - vector(persist.world.width / 2, persist.world.height / 2)

		for i = # self.entities, 1, -1 do
			local kill, new = ent:update(dt, map, player)
			if not kill then
				--AABB camera
				if util.collide(ent.pos, ent.box.w, ent.box.h, min, persist.world.width, persist.world.height) then
					table.insert(self.entityDraw, ent)
				end
				--new ents
				for _, e in ipairs(new) do
					table.insert(self.entities, e)
				end
			else
				table.remove(self.entities, i)
			end
		end
	end,

	drawEntities = function(self)
		for _, ent in ipairs(self.entityDraw) do
			ent:draw()
		end
	end,

	--camera funcs
	updateCam = function(self)
		--dostuff
	end,

	keypressed = function(self, key, unicode)
		if key == "escape" then love.event.push("quit") else love.event.push("changeState", "test") end
	end
}

return Game