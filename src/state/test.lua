--[[
	Game state.
	Main gameplay state.
]]

class "TestState" (State) {
	__init__ = function(self)
		--world config
		self.tileSet = cache.tileset("platformy.tset")

		--load world
		-- self.map = self.loadMap("test2.map")
		local map = persist.mapName and cache.map(persist.mapName) or cache.map("test3.map")
		self.map = map

		-- local playerStart = self.map:getPlayerStart()
		-- self.player = Player(playerStart)
		self.player = multiCharacter(2, Character("solid.spr", vector(16, 16)), Character("fullsuit.spr", vector(16, 16)))
		-- self.entity = self.map:spawnEntities()
		self.entity = {Entity("fullsuit.spr", vector(16, 16))}
		self.entityDraw = {}
		self.ui = {
			hp = self.player.hp, 
			hpImg = cache.image("gfx/hp.png"),
			hpColor = {255, 255, 255}
		}

		self.cam = vector()
		local pw = self.player.box.w / 2 * config.scale
		local ph = self.player.box.h / 2 * config.scale
		self.camOffset = vector(config.screen.width / 2 - pw, config.screen.height / 2 - ph)
		self.camOffset = self.camOffset / config.scale

		love.graphics.setBackgroundColor(50, 150, 200)

		--controller
		self.padList = love.joystick.getJoysticks()
		if next(self.padList) then 
			self.pad = self.padList[1]
		end
	end,

	update = function(self, dt)
		-- dt = dt * 0.25
		--input
		-- self.player.control.up = self.pad:isGamepadDown("dpup")
		-- self.player.control.down = self.pad:isGamepadDown("dpdown")
		-- self.player.control.left = self.pad:isGamepadDown("dpleft")
		-- self.player.control.right = self.pad:isGamepadDown("dpright")
		self.player.control.up = love.keyboard.isDown("w", "up")
		self.player.control.down = love.keyboard.isDown("s", "down")
		self.player.control.left = love.keyboard.isDown("a", "left")
		self.player.control.right = love.keyboard.isDown("d", "right")

		self.entity[1].vel.x = 50

		--update
		self.map:update(dt)
		self:updateEntities(dt, self.map, self.player)
		self.player:update(dt, self.map)
		self:updateUI()

		self:updateCam()
	end,

	draw = function(self)
		love.graphics.push()
		love.graphics.scale(config.scale)
		love.graphics.push()
		love.graphics.translate(self.cam.x, self.cam.y)
			--draw previous map if scrolling to new area
			if self.scrollMap then
				self.scrollMap:draw()
			end
			self.map:draw() --draw <= walkable layer
			self:drawEntities()
			self.player:draw()
			self.map:drawForeground()
		love.graphics.pop()
		--draw hud
		self:drawUI()
		love.graphics.pop()
	end,

	--entity funcs
	updateEntities = function(self, dt, map, player)
		self.entityDraw = {}
		local min = self.player.pos - vector(config.screen.width / 2, config.screen.height / 2)

		for i = # self.entity, 1, -1 do
			local ent = self.entity[i]
			local kill, new = ent:update(dt, map, player)

			if not kill then
				--AABB camera
				if util.collide(ent.pos, ent.box.w, ent.box.h, min, config.screen.width, config.screen.height) then
					table.insert(self.entityDraw, ent)
				end
			else
				table.remove(self.entities, i)
			end

			--new ents
			for _, e in ipairs(new) do
				table.insert(self.entities, e)
			end
		end
	end,

	drawEntities = function(self)
		for _, ent in ipairs(self.entityDraw) do
			ent:draw()
		end
	end,

	--camera funcs
	updateCam = function(self)
		local tileSize = self.map:getTileSize()
		local mapW, mapH = self.map.width * tileSize - self.player.box.w, self.map.height * tileSize - self.player.box.h
		--pan X
		if self.player.pos.x > self.camOffset.x and self.player.pos.x < mapW - self.camOffset.x then
			self.cam.x = self.camOffset.x - self.player.pos.x
		end
		--pan Y
		if self.player.pos.y > self.camOffset.y and self.player.pos.y < mapH - self.camOffset.y then
			self.cam.y = self.camOffset.y - self.player.pos.y
		end
	end,

	updateUI = function(self)
		if self.ui.hp ~= self.player.hp then
			self.ui.hp = self.player.hp
		end
	end,

	drawUI = function(self)
		love.graphics.setColor(255, 255, 255, 255)
		for x = 1, self.ui.hp do
			love.graphics.draw(self.ui.hpImg, 12 * x, 8)
		end
	end,

	keypressed = function(self, key, unicode)
		if key == "escape" then love.event.push("quit") end

		--switch to editor
		if key == "f3" then
			love.event.push("changeState", "edit")
		end

		--controls(temporary)
		if key == " " and self.player:canJump() then
			self.player.control.jump = true
			line = nil
		elseif key == "q" then
			self.player:switchChar(1)
		elseif key == "e" then
			self.player:switchChar(2)
		end
	end,

	keyreleased = function(self, key)
		if key == " " then
			self.player.control.jumpRelease = true
		end
	end,

	gamepadpressed = function(self, pad, button)
		if button == "a" and self.player:canJump() then
			self.player.control.jump = true
		end
	end,

	gamepadreleased = function(self, pad, button)
		if button == "a" then
			self.player.control.jumpRelease = true
		end
	end
}

return TestState