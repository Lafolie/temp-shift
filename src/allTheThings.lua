--libs
require "lib.slither"
jupiter = require "lib.jupiter"
cache = require "lib.cache"
vector = require "lib.vector"
util = require "util"

--add dir requires here
local reqDir = {"class"}

--require everything inside the specified dirs
for _, dir in ipairs(reqDir) do
	local ls = love.filesystem.getDirectoryItems(dir)
	print "Requires:" --debug
	for __, fileName in ipairs(ls) do
		if fileName:match("^.+%.lua$") then --ignore bullshit
		print("\t" .. dir .."/" .. fileName) --debug
			require(dir .. "." ..  fileName:match("^(.+)%.lua$"))
		end
	end
end