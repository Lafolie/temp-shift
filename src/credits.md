# Platformy

*2D platformer engine built using the [Löve](https://love2d.org/) framework*

***

![Bleep](https://pbs.twimg.com/profile_images/2563355362/bleepTwitter.png)

## Version 0.1

### Main Programming
Dale James

### Bundled Assets
Dale James

## 3^rd Party Libraries

***

### Slither
Bart van Strien

### vector.lua from HUMP
Matthias Richter