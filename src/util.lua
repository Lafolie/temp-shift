--[[
	Util - Common utilities
	Procedures used by various classes or in multiple locations. DRY
]]
local util = {}

util.collide = function(a, aw, ah, b, bw, bh)
	return not (
		a.x + aw < b.x or
		a.y + ah < b.y or
		b.x + bw < a.x or
		b.y + bh < a.y
		)
end

util.collidePoint = function(a, aw, ah, b)
	return not (
		a.x < b.x or
		a.y < b.y or
		a.x + aw < b.x or
		a.y + ah < b.y
		)
end

util.assertArg = function(f, n, expected, arg)
	local t = type(arg)
	local str = {"Bad argument #", n, " to '", f, "' (", expected, " expected, got ", t, " of value '", tostring(arg), "')"}
	assert(arg and t == expected, table.concat(str))
end

util.clamp = function(l, v, h)
	return math.max(math.min(v, h), l)
end

return util