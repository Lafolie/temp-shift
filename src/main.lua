require "allTheThings"

--state handling
local stateList = {}
local curState = State()
love.handlers.changeState = function(s)
	assert(stateList[s], "Invalid state specified.")
	curState = stateList[s]()	
end


--loop
love.load = function()
	--löve stuff
	local width, height, flags = love.window.getMode()
    local dtopWidth, dtopHeight = love.window.getDesktopDimensions(flags.display)
    love.keyboard.setTextInput(false)
    
    --globals
    persist = {}
	config = {
		scale = 2, 
		scaleMode = "nearest", 
		screen = {width = width, height = height}, 
		dtop = {width = dtopwidth, height = dtopheight}
	} --add keybindings to config
	
	love.graphics.setDefaultFilter(config.scaleMode)

	--load states
	for _, file in ipairs(love.filesystem.getDirectoryItems("state")) do
		local state = file:match("^(.+)%.lua$")
		if state then
			stateList[state] = require("state." .. state)
		end
	end

	love.event.push("changeState", "test")
end

love.update = function(dt)
	curState:update(dt)
end

love.draw = function()
	curState:draw()
end

--m&k
love.textinput = function(text)
	curState:textinput(text)
end

love.keypressed = function(key, unicode)
	curState:keypressed(key, unicode)
end

love.keyreleased = function(key)
	curState:keyreleased(key)
end

love.mousepressed = function(x, y, button)
	curState:mousepressed(x, y, button)
end

love.mousereleased = function(x, y, button)
	curState:mousereleased(x, y, button)
end

love.mousefocus = function(f)
	curState:mousefocus(f)
end

--controller
love.joystickadded = function(joystick)
	curState:joystickadded(joystick)
end

love.joystickremoved = function(joystick)
	curState:joystickremoved(joystick)
end

love.gamepadpressed = function(joystick, button)
	curState:gamepadpressed(joystick, button)
end

love.gamepadreleased = function(joystick, button)
	curState:gamepadreleased(joystick, button)
end

--application
love.focus = function(f)
	curState:focus(f)
end

love.visible = function(v)
	curState:visible(v)
end

love.quit = function()
	curState:quit()
end